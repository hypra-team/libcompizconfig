Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libcompizconfig
Source: https://github.com/compiz-reloaded/libcompizconfig

Files: *
Copyright: 2007  Dennis Kasprzyk <onestone@opencompositing.org>
           2007  Danny Baumann <maniac@opencompositing.org>
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: src/iniparser.c src/iniparser.h
Copyright: Nicolas Devillard (ndevilla AT free DOT fr)
License: misc-1
 Permission is granted to anyone to use this software for any
 purpose on any computer system, and to redistribute it freely,
 subject to the following restrictions:
 .
 1. The author is not responsible for the consequences of use of
 this software, no matter how awful, even if they arise
 from defects in it.
 .
 2. The origin of this software must not be misrepresented, either
 by explicit claim or by omission.
 .
 3. Altered versions must be plainly marked as such, and must not
 be misrepresented as being the original software.
 .
 4. This notice may not be removed or altered.

Files: backend/ini.c
Copyright: 2007 Danny Baumann <maniac@opencompositing.org>
License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright: 2007 Sean Finney <seanius@debian.org>
           2016 Jof Thibaut <compiz@tuxfamily.org>
License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.
